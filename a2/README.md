
# LIS4368

## Kallayah Henderson 

### Assignment #2 Requirements:


1. Log-in to MySQL using AMPPS 
2. Write a welcome page 
3. Write a Hello-World java servlet-compile it correctly 
4. Configure Servlets request URL in "webapps\hello\WEB-INF\web.xml"
5. Invoke the servlet 
6. Write a database servlet
7. install MySQL JDBC driver- copy to Tomcats lib folder 
8. Write a client-side HTML form 
9. Write the server-side database query servlet- invoke from the client-side 
10. Configure the request URL for the Servlet 
11. Deploy servlet using @WebServlet 

#### README.md file should include the following items:
*Assesment Links:*

* http://localhost:9999/hello
* http://localhost:9999/hello/index.html
* http://localhost:9999/hello/sayhello
* http://localhost:9999/hello/querybook.html
* http://localhost:9999/hello/sayhi




#### Assignment Screenshots:

*Screenshot of results*:

![hello Screenshot](img/hello.png )
![index.html Screenshot](img/index.png )
![hello/sayhello Screenshot](img/sayhello.png )
![hello/querybook.html Screenshot](img/querybook.png )
![hello/sayhi Screenshot]( img/sayhi.png)
![query results Screenshot](img/query.png )
