# LIS4368

## Kallayah Henderson 

### Assignment #4 Requirements:
1. run tomcat server 
2. edit servlet files for basic server side validation
3. provide bitbucket read-only access with the following inside:
- img folder for failed validation and passed validation png's
 




#### Assignment Screenshots and links:

*Screenshot A4  Failed Validation*:
![A4 screenshot]( img/basic.png "main page" )
![A4 screenshot](img/results.png "passed server side validation")

