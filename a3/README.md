# LIS4368

## Kallayah Henderson 

### Assignment #3 Requirements:

1. log-in to MySQL using AMPPS
2. Use MySQL workbench to create an ERD
3. create 10 records per table 
4. forward-engineer the ERD data design with data  locally 
5. provide bitbucket read-only access with the following inside:
- docs folder: a3.mwb and a3.sql 
- img folder for a3.png
- README.md with ERD screenshot included 
          



#### Assignment Screenshots and links:

*Screenshot A3  ERD*:
![A3 ERD]( img/a3.png "ERD based upon A3 Requirements" )

*A3 docs: a3.mwb and a3.sql*:

[A3 MWB File](docs/A3.mwb "A3 ERD in .mwb format")

[A3 SQL File](docs/A3.sql "A3 SQL Script")
