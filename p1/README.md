# LIS4368

## Kallayah Henderson 

### Project #1 Requirements:

1. log-in to AMPPS
2. Start tomcat server 
3. edit index.jsp file 
- modify meta tags
- edit titles and images
4. add form controls to match attributes of customer entity 

#### Assignment Screenshots and links:

*Screenshot home and p1 page*:
![p1 ]( img/portfolio.png "home page for local host website" )

![p1](img/validation.png "data validaton of customer")


