# LIS4368

## Kallayah Henderson 

### Project #2 Requirements:

1. log-in to AMPPS
2. Start tomcat server 
3. complete the JSP/Servlets web application using the MVC framework
 - providing create, read, updateand delete(CRUD) functionality

#### Assignment Screenshots and links:

*Screenshot validation of data entry, display and modification pages*:
![p2 ]( img/first.png "home page for local host website" )

![p2](img/second.png "data validaton of customer")

![p2](img/third.png "data ")

![p2](img/fourth.png "update of customer")

![p2](img/fifth.png "update of customer table")

![p2](img/sixth.png "select and update")

![p2](img/seventh.png "insert and delete")
