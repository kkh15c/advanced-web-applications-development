# LIS4368- Advanced Web Applications 
## Kallayah Henderson 

## LIS4368 Requirments: 

*Course Work Links:*

1.  [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK 

    - Install Tomcat 

    - Provide screenshots 

    - Create Bitbuck tutorials 

     (bitbucketstationlocations and myteamquotes)
 
    - Provide git command descriptions 

1.  [A2 README.md](a2/README.md "My A2 README.md file")

    - Log-in to MySQL using AMPPS 

    - Write a welcome page

    - Write a Hello-World java servlet-compile it correctly 

    - Configure Servlets request URL in "webapps\hello\WEB-INF\web.xml"

    - Invoke the servlet 

    - Write a database servlet

    - install MySQL JDBC driver- copy to Tomcats lib folder

    -  Write a client-side HTML form
 
    - Write the server-side database query servlet- invoke from the client-side 

    -  Configure the request URL for the Servlet 

    - Deploy servlet using @WebServlet 

1.  [A3 README.md](a3/README.md "My A3 README.md file")
    
    - log-in to MySQL using AMPPS
    - Use MySQL workbench to create an ERD
    - create 10 records per table 
    - forward-engineer the ERD data design with data  locally 
    
1.  [P1 README.md](p1/README.md "My p1 README.md file")
       
    - log-in to AMPPS
    - Start tomcat server 
    - edit index.jsp file 
    - modify meta tags
    - edit titles and images
    - add form controls to match attributes of customer entity 
    
1. [A4 README.md](a4/README.md "My A4 README.md file")
    - use tomcat server 
    - edit servlet files for basic server-side validation
    
1. [A5 README.md](a5/README.md "My A5 README.md file")
    - use tomcat server 
    - edit servlet files for basic server-side validation 
    - include new data file inside of admin 
    
1. [P2 README.md](p2/README.md "My P2 README.md file")
    - use tomcat server 
    - edit servlet files for basic server-side validation 
    - include new data file inside of admin
    
    
        
      

