# LIS4368- Advanced Web Applications Development 

## Kallayah Henderson 

### Assignment 1 Requirements: 

>#### three Parts:

1. Distributed version control with git and bitbucket 
2. Install JDK 
3. Install Tomcat 
4. java/JSP/Servlet development and installation 
5. Chapter questions (chp 1-4)



#### README.md file should include the following items:

* Screen shot of running java.hello(number 2 above)
* screenshot of running http://localhost:9999 (number 3 above)
* link to local lis4368 web app: http://localhost:9999/lis4368/index.jsp 
* git commands with short descriptions 
* Bitbucket repo links: a) this assignment and b) the completed tutorial above bitbucketstationlocations)

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - create new local repository 
2. git status- lists the files you have changed and those you still need to add or commit 
3. git add- adds one or more files 
4. git commit- commit changes to head 
5. git push- send changes to the master branch of your repository 
6. git pull-fetch and merge changes on the remote server to your working directory 
7. git push origin master- send changes to the master branch of your repository 

#### Assignment Screenshots:

*Screenshot of Tomcat running http://localhost*:

![Tomcat Installation Screenshot](img/tomcat.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/kkh15c/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/kkh15c/mywebproject/src/master/tomcat/webapps/lis4368/a1/ "My Team Quotes Tutorial")
