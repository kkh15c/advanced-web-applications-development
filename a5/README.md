# LIS4368

## Kallayah Henderson 

### Assignment #5 Requirements:
1. run tomcat server 
2. edit servlet files for basic server side validation
3. provide bitbucket read-only access with the following inside:
- img folder for passed validation and mysql data table with new entry





#### Assignment Screenshots and links:

*Screenshot A4  Failed Validation*:
![A5 screenshot]( img/first.png "main page" )
![A5 screenshot](img/results.png "passed server side validation")
![A5 screenshot](img/mysql.png "sql tables")

